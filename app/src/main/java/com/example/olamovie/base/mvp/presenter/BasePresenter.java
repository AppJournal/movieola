package com.example.olamovie.base.mvp.presenter;

import com.example.olamovie.base.mvp.view.BaseView;

/**
 * Created by Kushal on 02,March,2020
 */
public interface BasePresenter<T extends BaseView> {

    /**
     * initialise the presenter and the view items on startup of the activity or fragment
     * @param view
     */
    void attachView(T view);

    /**
     *  use this method to clean up presenter
     */
    void onDestroy();

}
