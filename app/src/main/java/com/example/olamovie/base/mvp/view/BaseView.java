package com.example.olamovie.base.mvp.view;

/**
 * Created by Kushal on 02,March,2020
 */
public interface BaseView {

    void initPresenter();

    void destroy();

}