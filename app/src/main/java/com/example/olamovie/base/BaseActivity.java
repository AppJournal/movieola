package com.example.olamovie.base;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.example.olamovie.base.mvp.view.BaseView;

/**
 * Created by Kushal on 02,March,2020
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    protected abstract View attachLayoutView();

    protected abstract void initView();

    protected Context context;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        context = this;

        // super call
        super.onCreate(savedInstanceState);

        // setting content
        setContentView(attachLayoutView());

        initPresenter();

        // init views
        initView();
    }

}
