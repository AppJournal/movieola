package com.example.olamovie.repository.networking;

import com.example.olamovie.repository.networking.Interceptors.NetworkInterceptor;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kushal on 02,March,2020
 */
public class API {

    private static ApiService apiService;

    private API() {
        // NO Implementation
    }

    public static ApiService getInstance(){

        if (apiService == null) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiService.BASE_URL)
                    .client(getHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            apiService = retrofit.create(ApiService.class);
        }

        return apiService;
    }

    /**
     * Constructs an OkHTTP client with network interceptor for inserting api key
     * @return OkHttpClient
     */
    private static OkHttpClient getHttpClient() {

        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new NetworkInterceptor())
                .build();
    }

}
