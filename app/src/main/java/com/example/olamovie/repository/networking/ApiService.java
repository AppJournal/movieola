package com.example.olamovie.repository.networking;

import com.example.olamovie.repository.networking.responses.getMovieList.MovieList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Kushal on 02,March,2020
 */
public interface ApiService {

    String BASE_URL = "https://api.themoviedb.org/3/";
    String API_KEY = "561e70f814404af5a766aea82a6bda4a";
    String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

    @GET("movie/popular")
    Call<MovieList> fetchMovieList();


}
