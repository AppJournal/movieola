package com.example.olamovie.repository.networking;

import android.text.TextUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kushal on 02,March,2020
 */
public abstract class RestCallback<T> implements Callback<T> {

    public abstract void onSuccess(T t, retrofit2.Response response);

    public abstract void onFailure(String apiError);

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        int code = response.code();
        String message = response.message();
        if(response.isSuccessful()){
            onSuccess(response.body(), response);
        }else {
            onFailure("Request Failed");
        }

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {

        String apiError = "Request Failed";
        if (t instanceof IOException) {

            apiError = "Please check your network connection";
            onFailure(apiError);
        } else if (t instanceof IllegalArgumentException) {
            String msg = t.getMessage();
            if (!TextUtils.isEmpty(msg)) {
                apiError = t.getMessage();
                onFailure(apiError);
            } else {
                apiError = "Something went wrong";
                onFailure(apiError);
            }
        } else {
            apiError = "Unexpected Error";
            onFailure(apiError);
        }
    }
}
