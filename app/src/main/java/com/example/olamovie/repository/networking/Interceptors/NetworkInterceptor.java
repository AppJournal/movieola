package com.example.olamovie.repository.networking.Interceptors;

import com.example.olamovie.repository.networking.ApiService;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kushal on 02,March,2020
 */
public class NetworkInterceptor implements Interceptor {

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {

        //Intercept original request and append a new query to add api_key along
        Request request = chain.request();

        HttpUrl originalHttpUrl = request.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("api_key", ApiService.API_KEY)
                .build();

        Request.Builder requestBuilder = request.newBuilder()
                .url(url);

        Request requestUpdated = requestBuilder.build();

        return chain.proceed(requestUpdated);
    }

}
