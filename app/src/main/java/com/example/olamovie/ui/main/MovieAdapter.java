package com.example.olamovie.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.olamovie.R;
import com.example.olamovie.repository.networking.ApiService;
import com.example.olamovie.repository.networking.responses.getMovieList.Result;

import java.util.List;


/**
 * Created by Kushal on 02,March,2020
 */
public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieVH> {

    private List<Result> mMovieList;
    private Context mContext;
    private static onMovieClickListener mOnMovieClickListener;

    public MovieAdapter(Context context , List<Result> movieList, onMovieClickListener OnMovieClickListener) {
        this.mMovieList = movieList;
        this.mContext = context;
        this.mOnMovieClickListener = OnMovieClickListener;
    }

    @NonNull
    @Override
    public MovieVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        int layoutId = R.layout.layout_movie_view;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutId, parent, false);

        return new MovieVH(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MovieVH holder, int position) {

        String posterUrl = ApiService.IMAGE_BASE_URL + mMovieList.get(position).getPosterPath();
        holder.movie_title.setText(mMovieList.get(position).getOriginalTitle());
        Glide.with(mContext).load(posterUrl)
                .placeholder(R.drawable.ic_placeholder_movies_black)
                .error(R.drawable.ic_placeholder_movies_black)
                .into(holder.movie_poster);
    }

    @Override
    public int getItemCount() {
        return mMovieList.size();
    }

    static class MovieVH extends RecyclerView.ViewHolder implements View.OnClickListener{

        private AppCompatImageView movie_poster;
        private AppCompatTextView movie_title;

        MovieVH(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            movie_title = itemView.findViewById(R.id.movie_name);
            movie_poster = itemView.findViewById(R.id.movie_poster);
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            mOnMovieClickListener.onMovieClicked(position);
        }
    }

    public interface onMovieClickListener{
        void onMovieClicked(int position);
    }
}
