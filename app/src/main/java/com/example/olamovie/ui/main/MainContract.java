package com.example.olamovie.ui.main;

import com.example.olamovie.base.mvp.presenter.BasePresenter;
import com.example.olamovie.base.mvp.view.BaseView;
import com.example.olamovie.repository.networking.responses.getMovieList.Result;

import java.util.List;

/**
 * Created by Kushal on 02,March,2020
 */
public interface MainContract {

    interface View extends BaseView {
        void onMovieListFetched(List<Result> movieList);
        void displayLinearLayout();
        void displayGridLayout();
    }

    interface Presenter extends BasePresenter<MainContract.View> {
        void fetchMovies();
    }

}
