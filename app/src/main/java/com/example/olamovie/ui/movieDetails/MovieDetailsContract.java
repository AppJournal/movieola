package com.example.olamovie.ui.movieDetails;

import com.example.olamovie.base.mvp.presenter.BasePresenter;
import com.example.olamovie.base.mvp.view.BaseView;

/**
 * Created by Kushal on 02,March,2020
 */
public interface MovieDetailsContract {

    interface view extends BaseView{

    }

    interface presenter extends BasePresenter<MovieDetailsContract.view>{

    }
}
