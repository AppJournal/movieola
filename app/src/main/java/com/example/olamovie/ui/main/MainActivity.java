package com.example.olamovie.ui.main;


import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.example.olamovie.R;
import com.example.olamovie.base.BaseActivity;
import com.example.olamovie.databinding.ActivityMainBinding;
import com.example.olamovie.repository.networking.responses.getMovieList.Result;
import com.example.olamovie.ui.movieDetails.MovieDetails;
import com.example.olamovie.utils.AppConstants;

import java.util.List;

public class MainActivity extends BaseActivity implements MainContract.View, MovieAdapter.onMovieClickListener {

    private ActivityMainBinding viewBinding;

    private MainPresenter presenter;

    private List<Result> movieResults;

    private boolean toggle = true;


    @Override
    protected View attachLayoutView() {
        viewBinding = ActivityMainBinding.inflate(getLayoutInflater());
        return viewBinding.getRoot();
    }

    @Override
    protected void initView() {
        presenter.fetchMovies();
    }


    @Override
    public void initPresenter() {
        presenter = new MainPresenter();
        presenter.attachView(this);
    }

    @Override
    public void destroy() {

    }

    @Override
    public void onMovieListFetched(List<Result> movieList) {

        movieResults = movieList;

        displayGridLayout();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toggle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.new_game:
                toggleDisplayList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void toggleDisplayList() {
        toggle = !toggle;

        if (toggle) {

            displayGridLayout();

        } else {

            displayLinearLayout();

        }
    }

    @Override
    public void displayLinearLayout() {

        if (movieResults != null) {
            MovieAdapter adapter = new MovieAdapter(this, movieResults, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            viewBinding.movieRv.setLayoutManager(layoutManager);

            viewBinding.movieRv.setAdapter(adapter);
            viewBinding.movieRv.setHasFixedSize(true);
        }
    }

    @Override
    public void displayGridLayout() {

        if (movieResults != null) {

            MovieAdapter adapter = new MovieAdapter(this, movieResults, this);
            GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
            viewBinding.movieRv.setLayoutManager(layoutManager);

            viewBinding.movieRv.setAdapter(adapter);
            viewBinding.movieRv.setHasFixedSize(true);
        }

    }


    @Override
    public void onMovieClicked(int position) {

        if (movieResults != null) {
            Result result = movieResults.get(position);

            Intent intent = new Intent(this, MovieDetails.class);
            intent.putExtra(AppConstants.MOVIE_DETAILS, result);

            startActivity(intent);
        }
    }
}
