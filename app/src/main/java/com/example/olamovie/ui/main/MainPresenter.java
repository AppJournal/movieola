package com.example.olamovie.ui.main;

import android.util.Log;

import com.example.olamovie.repository.networking.API;
import com.example.olamovie.repository.networking.ApiService;
import com.example.olamovie.repository.networking.RestCallback;
import com.example.olamovie.repository.networking.responses.getMovieList.MovieList;

import retrofit2.Response;

/**
 * Created by Kushal on 02,March,2020
 */
public class MainPresenter implements MainContract.Presenter {


    private MainContract.View view;
    private ApiService api;

    MainPresenter() {
        api = API.getInstance();
    }

    @Override
    public void attachView(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void fetchMovies() {

        api.fetchMovieList().enqueue(new RestCallback<MovieList>() {
            @Override
            public void onSuccess(MovieList movieList, Response response) {

                if (view != null && movieList != null && movieList.getResults() != null){
                    view.onMovieListFetched(movieList.getResults());
                    System.out.println("onSucesss");
                }

            }

            @Override
            public void onFailure(String apiError) {

            }
        });
        
    }

    @Override
    public void onDestroy() {

    }


}
