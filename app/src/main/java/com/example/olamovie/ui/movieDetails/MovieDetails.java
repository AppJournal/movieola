package com.example.olamovie.ui.movieDetails;

import android.content.Intent;
import android.view.View;

import com.bumptech.glide.Glide;
import com.example.olamovie.R;
import com.example.olamovie.base.BaseActivity;
import com.example.olamovie.databinding.ActivityMovieDetailsBinding;
import com.example.olamovie.repository.networking.ApiService;
import com.example.olamovie.repository.networking.responses.getMovieList.Result;
import com.example.olamovie.utils.AppConstants;

/**
 * Created by Kushal on 02,March,2020
 */
public class MovieDetails extends BaseActivity implements MovieDetailsContract.view {

    private ActivityMovieDetailsBinding viewBinding;
    private Result movieResult;

    @Override
    protected View attachLayoutView() {
        viewBinding = ActivityMovieDetailsBinding.inflate(getLayoutInflater());
        return viewBinding.getRoot();
    }

    @Override
    protected void initView() {
        handleIntents();
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void destroy() {

    }

    private void handleIntents() {

        Intent readIntent = getIntent();

        if (readIntent.hasExtra(AppConstants.MOVIE_DETAILS)){
            movieResult = readIntent.getParcelableExtra(AppConstants.MOVIE_DETAILS);
            updateUi();
        }
    }

    private void updateUi() {

        if (movieResult == null) {
            return;
        }

        String backdropPath = ApiService.IMAGE_BASE_URL+movieResult.getBackdropPath();
        loadMoviePoster(backdropPath);

        //update movie name
        viewBinding.movieName.setText(movieResult.getTitle());

        //update movie overview
        viewBinding.movieOverview.setText(movieResult.getOverview());

    }

    private void loadMoviePoster(String backdropPath) {

        Glide.with(this).load(backdropPath)
                .error(R.drawable.ic_placeholder_movies_black)
                .placeholder(R.drawable.ic_placeholder_movies_black)
                .into(viewBinding.movieVideo);

    }
}
